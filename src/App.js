import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from './components/header/Header';
import AddStruk from './components/struk/AddStruk';
import ListStruk from './components/struk/ListStruk';
import EditStruk from './components/struk/EditStruk';
import Login from './components/login/Login';
import Utama from './components/dashboard/Utama';

class App extends Component {
  render() {
    return (
      <Switch>
        <Header/>
        <Route path='/' exact component={Utama} />
        <Route path='/login' component={Login} />
        <Route path='/listStruk' component={ListStruk} />
        <Route path='/addStruk' component={AddStruk} />
        {/* <Route path='/editStruk/:id' component={EditStruk} /> */}
      </Switch>
    );
  }
}

export default App;
