import React, { Component } from "react";
import './Header.css'

class Header extends Component {
  render() {
    return (
      <div>
        <div className="header">
          <a href="#default" className="logo">
            NexDry
          </a>
          <div className="header-right">
  <input type="text" name="search" placeholder="Search.."/>
            <a className="active" href='/listStruk'>
              Data
            </a>
            <a href='/login'>Login</a>
            <a href='/addStruk'>Tambah</a>
            </div>
        </div>
      </div>
    );
  }
}

export default Header;
