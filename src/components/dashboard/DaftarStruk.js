// import React, { Component } from 'react'
// import axios from 'axios'

// export class DaftarStruk extends Component {
//     constructor() {
// 		super();
// 		this.state = {
// 			struks: [],
// 		};
//     }
//     componentDidMount() {
// 		axios
// 			.get(`http://localhost:8080/struks`)
// 			.then((res) => {
// 				const struks = res.data;
// 				this.setState({ struks });
// 			})
// 			.catch((error) => {
// 				console.log(error);
// 			});
// 	}
//     render() {
//         return (
//             <div>
//                 <div className="body">
// 					<h2>Daftar Pemesanan NexLaundry</h2>
// 					<div style={{ overflowX: 'auto' }}>
// 						<table>
// 							<thead>
// 								<tr>
// 									<th>No</th>
// 									<th>Nama</th>
// 									<th>Berat</th>
// 									<th>Bayar</th>
// 								</tr>
// 							</thead>
// 							<tbody>
// 								{this.state.struks.length ? (
// 									this.state.struks.map((struk, i) => (
// 										<tr>
// 											<td>{i + 1}</td>
// 											<td>{struk.nama}</td>
// 											<td>{struk.berat}</td>
// 											<td>{struk.bayar}</td>
// 										</tr>
// 									))
// 								) : (
// 									<tr />
// 								)}
// 								<tr>
// 									<td />
// 									<td />
// 									<td />
// 									<td />
// 									<td />
// 								</tr>
// 							</tbody>
// 						</table>
// 					</div>
// 				</div>
//             </div>
//         )
//     }
// }

// export default DaftarStruk