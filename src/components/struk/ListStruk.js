import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';


export class ListBuku extends Component {
	constructor() {
		super();
		this.state = {
			struks: [],
			id: 0
		};
	}
	componentDidMount() {
		Axios
			.get(`http://localhost:8080/struks`)
			.then((res) => {
				const struks = res.data;
				this.setState({ struks });
			})
			.catch((error) => {
				console.log(error);
			});
	}

	delete = (id) => {
		Axios
			.delete('http://localhost:8080/deleteStruk/' + id)
			.then(() => {
				window.location.reload();
			})
			.catch((error) => {
				console.log(error);
			});
	};

	edit = (id) => {
		console.log(id);
		this.setState({
			id
		});
	};

	render() {
		console.log('kkkkkk ' + this.state.id);
		return (
			<div>
				<div className="body">
					<Link to="/addStruk">
						<button className="btnAdd">Tambah Buku</button>
					</Link>
					<h2>Daftar Pemesanan NexDry</h2>
					<div style={{ overflowX: 'auto' }}>
						<table>
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Berat</th>
									<th>Bayar</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								{this.state.struks.length ? (
									this.state.struks.map((struk, i) => (
										<tr>
											<td>{i + 1}</td>
											<td>{struk.nama}</td>
											<td>{struk.berat}</td>
											<td>{struk.bayar}</td>
											<td>
												{/* <button className="btnEdit" onClick={() => {this.edit(buku.id)}}>Edit</button> */}
												<Link to={'/editStruk/' + struk.id}>
													<button
														className="btnEdit"
														struk={this.state.struks}
														key={this.state.struks.id}
													>
														Edit
													</button>{' '}
												</Link>
												<button
													className="btnDelete"
													onClick={() => {
														if (window.confirm('Yakin ingin menghapus data?')) {
															this.delete(struk.id);
														}
													}}
												>
													Hapus
												</button>
											</td>
										</tr>
									))
								) : (
									<tr />
								)}
								<tr>
									<td />
									<td />
									<td />
									<td />
									<td />
									<td />
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		);
	}
}

export default ListBuku;