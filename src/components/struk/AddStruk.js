import React, { Component } from 'react'
import Axios from 'axios';

export default class AddStruk extends Component {
  constructor(props) {
		super(props);
		this.state = {
			nama: '',
			berat: '',
			bayar: ''
		};
	}
	handleChange = (event) => {
		this.setState({ [event.target.name]: event.target.value });
	};

	handleSubmit = (tambah) => {
		tambah.preventDefault();
        Axios.post('http://localhost:8080/addStruk', this.state)
        this.props.history.push('/listStruk')
        window.location.reload()
	};
	render() {
		return (
			<div>
        <form onSubmit={this.handleSubmit} className="card">
      <h2 className="text1">Tambahkan Pesanan</h2>
      <p>
        <label className="text">
          <b>Nama</b>
        </label>
        <br />
        <input
          className="w3-input w3-border animasi"
          name="nama"
          value={this.state.nama}
          onChange={this.handleChange}
          type="text"
          style={{ width: 150 }}
        />
      </p>
      <p>
        <label className="text">
          <b>Berat</b>
        </label>
        <br />
        <input
          className="w3-input w3-border animasi"
          name="berat"
          onChange={this.handleChange}
          type="text"
          style={{ width: 150 }}
        />
        <br />
        </p>
        <p>

        <label className="text">
          <b>Bayar</b>
        </label>
        <br />
        <input
          className="w3-input w3-border animasi"
          name="bayar"
          onChange={this.handleChange}
          type="text"
          style={{ width: 150 }}
        />
      </p>
      <p>
  <input type="submit" value="Tambah" />
				
      </p>
    </form>
</div>


		);
	}
}